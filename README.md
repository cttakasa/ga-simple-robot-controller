# A Simple Genetic Algorithm Defined Robot Controller

This code provides an example of the selection process for deciding on the encoding of a simple reactive agent's movement within some environment.

Simple means the environment is tiled, and 2-D
